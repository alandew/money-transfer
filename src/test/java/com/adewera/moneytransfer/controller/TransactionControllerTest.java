package com.adewera.moneytransfer.controller;

import com.adewera.moneytransfer.dto.TransactionDto;
import com.adewera.moneytransfer.model.Currency;
import com.adewera.moneytransfer.model.Money;
import com.adewera.moneytransfer.model.Transaction;
import com.adewera.moneytransfer.service.TransactionService;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class TransactionControllerTest {
    private static final String TRANSACTIONS_ENDPOINT = "/transactions";
    private static final TransactionService transactionService = mock(TransactionService.class);

    @ClassRule
    public static final ResourceTestRule resources =
            ResourceTestRule.builder().addResource(new TransactionController(transactionService)).build();

    private TransactionDto inputTransactionDto;
    private Transaction outputTransaction;

    @Before
    public void setUp() {
        inputTransactionDto = new TransactionDto();
        inputTransactionDto.setWalletIdFrom(1L);
        inputTransactionDto.setWalletIdTo(2L);
        inputTransactionDto.setCurrency("USD");
        inputTransactionDto.setAmount(BigDecimal.ONE.setScale(2));

        outputTransaction = new Transaction();
        outputTransaction.setId(1L);
        outputTransaction.setWalletIdFrom(1L);
        outputTransaction.setWalletIdTo(2L);
        outputTransaction.setMoney(new Money(Currency.USD, BigDecimal.ONE.setScale(2)));
    }

    @After
    public void tearDown() {
        reset(transactionService);
    }

    @Test
    public void createTransaction() throws Exception {
        //given
        Entity<TransactionDto> entity = Entity.entity(inputTransactionDto, MediaType.APPLICATION_JSON_TYPE);
        when(transactionService.createTransaction(any())).thenReturn(1L);

        //when
        Response response = resources.target(TRANSACTIONS_ENDPOINT).request()
                .post(entity);

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.CREATED);
        assertThat(response.getLocation().getPath()).isEqualTo(TRANSACTIONS_ENDPOINT + "/1");
        verify(transactionService).createTransaction(any());
    }

    @Test
    public void getTransaction() throws Exception {
        //given
        when(transactionService.getTransaction(1L)).thenReturn(outputTransaction);

        //when
        Response response = resources.target(TRANSACTIONS_ENDPOINT + "/1").request().get();

        //then
        TransactionDto transactionDto = response.readEntity(TransactionDto.class);
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
        assertThat(transactionDto.getId()).isEqualTo(1L);
        assertThat(transactionDto.getWalletIdFrom()).isEqualTo(1L);
        assertThat(transactionDto.getWalletIdTo()).isEqualTo(2L);
        assertThat(transactionDto.getCurrency()).isEqualTo("USD");
        assertThat(transactionDto.getAmount()).isEqualTo(BigDecimal.ONE.setScale(2));
        verify(transactionService).getTransaction(eq(1L));
    }

    @Test
    public void getTransactions() throws Exception {
        //given
        when(transactionService.getTransactions(1L, 2L)).thenReturn(Collections.emptyList());

        //when
        Response response = resources.target(TRANSACTIONS_ENDPOINT).
                queryParam("from-wallet", 1L).
                queryParam("to-wallet", 2L).
                request().get();

        //then
        List<TransactionDto> returnedTransactions = response.readEntity(List.class);
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
        assertThat(returnedTransactions.size()).isEqualTo(0);
        verify(transactionService).getTransactions(eq(1L), eq(2L));
    }

}