package com.adewera.moneytransfer.controller;

import com.adewera.moneytransfer.UnitTestMarker;
import com.adewera.moneytransfer.dto.UserDetailedDto;
import com.adewera.moneytransfer.dto.UserDto;
import com.adewera.moneytransfer.dto.WalletDto;
import com.adewera.moneytransfer.model.User;
import com.adewera.moneytransfer.service.UserService;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestMarker.class)
public class UserControllerTest {
    private static final String USERS_ENDPOINT = "/users";
    private static final UserService userService = mock(UserService.class);

    @ClassRule
    public static final ResourceTestRule resources =
            ResourceTestRule.builder().addResource(new UserController(userService)).build();

    @After
    public void tearDown() {
        reset(userService);
    }

    @Test
    public void testCreateUser() {
        //given
        UserDto input = new UserDto();
        input.setName("Alan");
        input.setSurname("Dewera");
        input.setEmail("alan.dewera@gmail.com");
        Entity<UserDto> entity = Entity.entity(input, MediaType.APPLICATION_JSON_TYPE);
        when(userService.createUser(any())).thenReturn(1L);

        //when
        Response response = resources.target(USERS_ENDPOINT).request()
                .post(entity);

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.CREATED);
        assertThat(response.getLocation().getPath()).isEqualTo(USERS_ENDPOINT + "/1");
        verify(userService).createUser(any());
    }

    @Test
    public void testGetUser() {
        //given
        User user = new User("Alan", "Dewera", "alan.dewera@gmail.com");
        user.setId(1L);
        when(userService.getUser(1L)).thenReturn(user);

        //when
        Response response = resources.target(USERS_ENDPOINT + "/1").request().get();

        //then
        UserDetailedDto userResponse = response.readEntity(UserDetailedDto.class);
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
        assertThat(userResponse.getId()).isEqualTo(1L);
        assertThat(userResponse.getName()).isEqualTo("Alan");
        assertThat(userResponse.getSurname()).isEqualTo("Dewera");
        assertThat(userResponse.getEmail()).isEqualTo("alan.dewera@gmail.com");
        assertThat(userResponse.getWallets()).isEmpty();
        verify(userService).getUser(eq(1L));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGetUsers() {
        //given
        User user1 = new User("Alan", "Dewera", "alan.dewera@gmail.com");
        user1.setId(1L);
        User user2 = new User("user2", "user2", "user2@gmail.com");
        user1.setId(2L);
        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        when(userService.getAllUsers()).thenReturn(users);

        //when
        Response response = resources.target(USERS_ENDPOINT).request().get();

        //then
        List<UserDto> returnedUsers = response.readEntity(List.class);
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
        assertThat(returnedUsers.size()).isEqualTo(2);
        verify(userService).getAllUsers();
    }

    @Test
    public void testUpdateUser() {
        //given
        UserDto input = new UserDto();
        input.setName("Alan");
        input.setSurname("Dewera");
        input.setEmail("alan.dewera@gmail.com");
        Entity<UserDto> entity = Entity.entity(input, MediaType.APPLICATION_JSON_TYPE);

        //when
        Response response = resources.target(USERS_ENDPOINT + "/1").request().put(entity);

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
        verify(userService).updateUser(eq(1L), any());
    }

    @Test
    public void testDeleteUsers() {
        //given

        //when
        Response response = resources.target(USERS_ENDPOINT + "/1").request().delete();

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
        verify(userService).deleteUser(eq(1L));
    }

    @Test
    public void testCreateWallet() {
        //given
        WalletDto input = new WalletDto();
        input.setAmount(BigDecimal.valueOf(10.00).setScale(2));
        input.setCurrency("PLN");
        Entity<WalletDto> entity = Entity.entity(input, MediaType.APPLICATION_JSON_TYPE);
        when(userService.createWallet(eq(1L), any())).thenReturn(2L);

        //when
        Response response = resources.target(USERS_ENDPOINT + "/1/wallets").request().post(entity);

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.CREATED);
        assertThat(response.getLocation().getPath()).isEqualTo(USERS_ENDPOINT + "/1/wallets/2");
        verify(userService).createWallet(eq(1L), any());
    }

    @Test
    public void testDepositWallet() {
        //given
        WalletDto input = new WalletDto();
        input.setAmount(BigDecimal.valueOf(10.00).setScale(2));
        input.setCurrency("PLN");
        Entity<WalletDto> entity = Entity.entity(input, MediaType.APPLICATION_JSON_TYPE);

        //when
        Response response = resources.target(USERS_ENDPOINT + "/1/wallets/1").request().put(entity);

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
        verify(userService).deposit(eq(1L), eq(1L), any());
    }

    @Test
    public void testDeleteWallet() {
        //given

        //when
        Response response = resources.target(USERS_ENDPOINT + "/1/wallets/1").request().delete();

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
        verify(userService).deleteWallet(eq(1L), eq(1L));
    }
}