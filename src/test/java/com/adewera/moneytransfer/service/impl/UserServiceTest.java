package com.adewera.moneytransfer.service.impl;

import com.adewera.moneytransfer.UnitTestMarker;
import com.adewera.moneytransfer.dto.UserDto;
import com.adewera.moneytransfer.dto.WalletDto;
import com.adewera.moneytransfer.model.Currency;
import com.adewera.moneytransfer.model.Money;
import com.adewera.moneytransfer.model.User;
import com.adewera.moneytransfer.model.Wallet;
import com.adewera.moneytransfer.repository.UserRepository;
import com.adewera.moneytransfer.service.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.ArgumentCaptor;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@Category(UnitTestMarker.class)
public class UserServiceTest {

    private UserService tested;
    private static final UserRepository userRepository = mock(UserRepository.class);

    @Before
    public void setUp() {
        tested = new UserService(userRepository);
    }

    @After
    public void tearDown() {
        reset(userRepository);
    }

    @Test
    public void createUserTest() {
        //given
        User user = new User();
        user.setId(1L);
        when(userRepository.saveUser(any())).thenReturn(user);

        //when
        Long id = tested.createUser(new UserDto());

        //then
        assertThat(id).isEqualTo(user.getId());
    }

    @Test
    public void getUserTestPositive() {
        //given
        User user = new User();
        user.setId(1L);
        when(userRepository.findUser(1L)).thenReturn(Optional.of(user));

        //when
        User result = tested.getUser(1L);

        //then
        assertThat(result.getId()).isEqualTo(1L);
    }

    @Test(expected = NotFoundException.class)
    public void getUserTestNegative() {
        //given
        when(userRepository.findUser(1L)).thenReturn(Optional.empty());

        //when
        tested.getUser(1L);

        //then Resource Not found exception
    }

    @Test(expected = BadRequestException.class)
    public void createWalletWalletAlreadyExistTest() {
        //given
        WalletDto walletDto = new WalletDto();
        walletDto.setCurrency("BTC");
        Wallet wallet = new Wallet();
        wallet.setMoney(new Money(Currency.BTC, BigDecimal.ONE.setScale(2)));
        User user = new User();
        user.setId(1L);
        user.addWallet(wallet);
        when(userRepository.findUser(1L)).thenReturn(Optional.of(user));

        //when
        tested.createWallet(1L, walletDto);

        //then BadRequestException exception
    }

    @Test
    public void createWalletWalletPositiveTest() {
        //given
        WalletDto walletDto = new WalletDto();
        walletDto.setCurrency("USD");
        Wallet wallet = new Wallet();
        wallet.setMoney(new Money(Currency.BTC, BigDecimal.ONE.setScale(2)));
        User user = new User();
        user.setId(1L);
        user.addWallet(wallet);
        when(userRepository.findUser(1L)).thenReturn(Optional.of(user));

        //when
        tested.createWallet(1L, walletDto);

        //then
        verify(userRepository).update(user);
    }

    @Test(expected = NotFoundException.class)
    public void deleteWalletWalletNegativeTest() {
        //given
        User user = new User();
        user.setId(1L);
        Wallet wallet = new Wallet();
        wallet.setId(1L);
        user.addWallet(wallet);
        when(userRepository.findUser(1L)).thenReturn(Optional.of(user));

        //when
        tested.deleteWallet(1L, 2L);

        //then NotFoundException
    }

    @Test
    public void deleteWalletWalletPositiveTest() {
        //given
        User user = new User();
        user.setId(1L);
        Wallet wallet = new Wallet();
        wallet.setId(1L);
        user.addWallet(wallet);
        when(userRepository.findUser(1L)).thenReturn(Optional.of(user));

        //when
        tested.deleteWallet(1L, 1L);

        //then
        verify(userRepository).update(user);
    }

    @Test(expected = BadRequestException.class)
    public void depositIncompatibleCurrencyTest() {
        //given
        User user = new User();
        user.setId(1L);
        Wallet wallet = new Wallet();
        wallet.setId(1L);
        wallet.setMoney(new Money(Currency.BTC, BigDecimal.ONE.setScale(2)));
        user.addWallet(wallet);
        WalletDto walletDto = new WalletDto();
        walletDto.setCurrency("PLN");
        when(userRepository.findUser(1L)).thenReturn(Optional.of(user));

        //when
        tested.deposit(1L, 1L, walletDto);

        //then BadRequestException
    }

    @Test
    public void depositTest() {
        //given
        User user = new User();
        user.setId(1L);
        Wallet wallet = new Wallet();
        wallet.setId(1L);
        wallet.setMoney(new Money(Currency.BTC, BigDecimal.ONE.setScale(2)));
        user.addWallet(wallet);
        WalletDto walletDto = new WalletDto();
        walletDto.setCurrency("BTC");
        walletDto.setAmount(BigDecimal.ONE.setScale(2));
        when(userRepository.findUser(1L)).thenReturn(Optional.of(user));
        ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);

        //when
        tested.deposit(1L, 1L, walletDto);

        //then
        verify(userRepository).update(argument.capture());
        assertThat(argument.getValue().getWallets().get(0).getMoney().getAmount()).
                isEqualTo(BigDecimal.valueOf(2).setScale(2));

    }
}