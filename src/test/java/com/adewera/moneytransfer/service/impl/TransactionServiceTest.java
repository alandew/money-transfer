package com.adewera.moneytransfer.service.impl;

import com.adewera.moneytransfer.UnitTestMarker;
import com.adewera.moneytransfer.dto.TransactionDto;
import com.adewera.moneytransfer.model.*;
import com.adewera.moneytransfer.repository.TransactionRepository;
import com.adewera.moneytransfer.repository.UserRepository;
import com.adewera.moneytransfer.service.TransactionService;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.ArgumentCaptor;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@Category(UnitTestMarker.class)
public class TransactionServiceTest {

    private TransactionService tested;
    private static final TransactionRepository transactionRepository = mock(TransactionRepository.class);
    private static final UserRepository userRepository = mock(UserRepository.class);
    private static final SessionFactory sessionFactory = mock(SessionFactory.class);
    private static final Session session = mock(Session.class);
    @Before
    public void setUp() {
        tested = new TransactionService(transactionRepository, userRepository, sessionFactory);
    }

    @After
    public void tearDown() {
        reset(userRepository);
        reset(transactionRepository);
        reset(sessionFactory);
        reset(session);
    }

    @Test(expected = NotFoundException.class)
    public void createTransactionTestNoUserWithWalletId() throws Exception {
        //given
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setWalletIdFrom(1L);
        when(userRepository.findByWalletId(eq(1L))).thenReturn(Optional.empty());

        //when
        tested.createTransaction(transactionDto);

        //then NotFoundException
    }

    @Test(expected = BadRequestException.class)
    public void createTransactionTestIncompatibleCurrencies() throws Exception {
        //given
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setWalletIdFrom(1L);
        transactionDto.setWalletIdTo(2L);
        transactionDto.setCurrency("USD");
        User fromUser = new User();
        fromUser.setId(1L);
        Wallet fromWallet = new Wallet();
        fromWallet.setId(1L);
        fromWallet.setMoney(new Money(Currency.PLN, BigDecimal.ONE));
        fromUser.addWallet(fromWallet);
        User toUser = new User();
        toUser.setId(2L);
        Wallet toWallet = new Wallet();
        toWallet.setId(2L);
        toWallet.setMoney(new Money(Currency.PLN, BigDecimal.ONE));
        toUser.addWallet(toWallet);
        when(userRepository.findByWalletId(eq(1L))).thenReturn(Optional.of(fromUser));
        when(userRepository.findByWalletId(eq(2L))).thenReturn(Optional.of(toUser));
        when(sessionFactory.getCurrentSession()).thenReturn(session);
        when(session.get(eq(User.class), eq(1L), eq(LockMode.PESSIMISTIC_WRITE))).thenReturn(fromUser);
        when(session.get(eq(User.class), eq(2L), eq(LockMode.PESSIMISTIC_WRITE))).thenReturn(toUser);

        //when
        tested.createTransaction(transactionDto);

        //then BadRequestException
    }

    @Test(expected = BadRequestException.class)
    public void createTransactionTestNotEnoughSupply() throws Exception {
        //given
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setWalletIdFrom(1L);
        transactionDto.setWalletIdTo(2L);
        transactionDto.setCurrency("PLN");
        transactionDto.setAmount(BigDecimal.ONE);
        User fromUser = new User();
        fromUser.setId(1L);
        Wallet fromWallet = new Wallet();
        fromWallet.setId(1L);
        fromWallet.setMoney(new Money(Currency.PLN, BigDecimal.ZERO));
        fromUser.addWallet(fromWallet);
        User toUser = new User();
        toUser.setId(2L);
        Wallet toWallet = new Wallet();
        toWallet.setId(2L);
        toWallet.setMoney(new Money(Currency.PLN, BigDecimal.ONE));
        toUser.addWallet(toWallet);
        when(userRepository.findByWalletId(eq(1L))).thenReturn(Optional.of(fromUser));
        when(userRepository.findByWalletId(eq(2L))).thenReturn(Optional.of(toUser));
        when(sessionFactory.getCurrentSession()).thenReturn(session);
        when(session.get(eq(User.class), eq(1L), eq(LockMode.PESSIMISTIC_WRITE))).thenReturn(fromUser);
        when(session.get(eq(User.class), eq(2L), eq(LockMode.PESSIMISTIC_WRITE))).thenReturn(toUser);

        //when
        tested.createTransaction(transactionDto);

        //then BadRequestException
    }

    @Test
    public void createTransactionTestPositiveCase() throws Exception {
        //given
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setWalletIdFrom(1L);
        transactionDto.setWalletIdTo(2L);
        transactionDto.setCurrency("PLN");
        transactionDto.setAmount(BigDecimal.ONE);
        User fromUser = new User();
        fromUser.setId(1L);
        Wallet fromWallet = new Wallet();
        fromWallet.setId(1L);
        fromWallet.setMoney(new Money(Currency.PLN, BigDecimal.ONE));
        fromUser.addWallet(fromWallet);
        User toUser = new User();
        toUser.setId(2L);
        Wallet toWallet = new Wallet();
        toWallet.setId(2L);
        toWallet.setMoney(new Money(Currency.PLN, BigDecimal.ONE));
        toUser.addWallet(toWallet);
        when(userRepository.findByWalletId(eq(1L))).thenReturn(Optional.of(fromUser));
        when(userRepository.findByWalletId(eq(2L))).thenReturn(Optional.of(toUser));
        when(sessionFactory.getCurrentSession()).thenReturn(session);
        when(session.get(eq(User.class), eq(1L), eq(LockMode.PESSIMISTIC_WRITE))).thenReturn(fromUser);
        when(session.get(eq(User.class), eq(2L), eq(LockMode.PESSIMISTIC_WRITE))).thenReturn(toUser);
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        ArgumentCaptor<Transaction> transactionCaptor = ArgumentCaptor.forClass(Transaction.class);

        //when
        tested.createTransaction(transactionDto);

        //then
        verify(transactionRepository).saveTransaction(transactionCaptor.capture());
        assertThat(transactionCaptor.getValue().getWalletIdFrom()).isEqualTo(1L);
        assertThat(transactionCaptor.getValue().getWalletIdTo()).isEqualTo(2L);
        assertThat(transactionCaptor.getValue().getMoney().getCurrency()).isEqualTo(Currency.PLN);
        assertThat(transactionCaptor.getValue().getMoney().getAmount()).isEqualTo(BigDecimal.ONE);
        verify(userRepository, atLeast(2)).saveUser(userArgumentCaptor.capture());
        assertThat(userArgumentCaptor.getAllValues().get(0).
                getWallets().get(0).getMoney().getAmount()).isEqualTo(BigDecimal.valueOf(0));
        assertThat(userArgumentCaptor.getAllValues().get(1).
                getWallets().get(0).getMoney().getAmount()).isEqualTo(BigDecimal.valueOf(2));

    }

    @Test(expected = NotFoundException.class)
    public void getTransactionNotFound() throws Exception {
        //given
        when(transactionRepository.findTransaction(eq(1L))).thenReturn(Optional.empty());

        //when
        tested.getTransaction(1L);

        //then NotFoundException
    }

    @Test
    public void getTransaction() throws Exception {
        //given
        Transaction transaction = new Transaction();
        transaction.setId(1L);
        when(transactionRepository.findTransaction(eq(1L))).thenReturn(Optional.of(transaction));

        //when
        Transaction result = tested.getTransaction(1L);

        //then
        assertThat(result.getId()).isEqualTo(1L);
    }

    @Test
    public void getTransactionsFromWalletIsNull() throws Exception {
        //given
        Long fromWalletId = null;
        Long toWalletId = 2L;

        //when
        tested.getTransactions(fromWalletId, toWalletId);

        //then
        verify(transactionRepository).findTransactionsTo(eq(2L));
    }

    @Test
    public void getTransactionsToWalletIsNull() throws Exception {
        //given
        Long fromWalletId = 1L;
        Long toWalletId = null;

        //when
        tested.getTransactions(fromWalletId, toWalletId);

        //then
        verify(transactionRepository).findTransactionsFrom(eq(1L));
    }

    @Test
    public void getTransactionsBothAreNull() throws Exception {
        //given
        Long fromWalletId = null;
        Long toWalletId = null;

        //when
        tested.getTransactions(fromWalletId, toWalletId);

        //then
        verify(transactionRepository).findAllTransactions();
    }

    @Test
    public void getTransactionsNoneAreNull() throws Exception {
        //given
        Long fromWalletId = 1L;
        Long toWalletId = 2L;

        //when
        tested.getTransactions(fromWalletId, toWalletId);

        //then
        verify(transactionRepository).findTransactionsFromTo(eq(1L), eq(2L));
    }


}