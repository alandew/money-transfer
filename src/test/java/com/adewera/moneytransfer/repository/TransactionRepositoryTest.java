package com.adewera.moneytransfer.repository;

import com.adewera.moneytransfer.UnitTestMarker;
import com.adewera.moneytransfer.model.*;
import io.dropwizard.testing.junit.DAOTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Category(UnitTestMarker.class)
public class TransactionRepositoryTest {
    @Rule
    public final DAOTestRule database = DAOTestRule.newBuilder().
            addEntityClass(Transaction.class).
            addEntityClass(Wallet.class).
            addEntityClass(User.class).
            build();

    private TransactionRepository tested;
    private Transaction transaction;

    @Before
    public void setUp() {
        tested = new TransactionRepository(database.getSessionFactory());
        transaction = new Transaction();
        transaction.setMoney(new Money(Currency.GBP, BigDecimal.ONE.setScale(2)));
        transaction.setWalletIdFrom(1L);
        transaction.setWalletIdTo(2L);
        transaction.setTimestamp(Timestamp.from(Instant.MIN));
    }


    @Test
    public void saveTransaction() throws Exception {
        //given

        //when
        Long userId = database.inTransaction(() -> tested.
                saveTransaction(transaction)).getId();

        //then
        assertThat(userId).isNotNull();
    }

    @Test
    public void findTransaction() throws Exception {
        //given

        //when
        Transaction transaction = database.inTransaction(() -> tested.
                findTransaction(3L)).get();

        //then
        assertThat(transaction.getMoney().getCurrency()).isEqualTo(Currency.BTC);
    }

    @Test
    public void findAllTransactions() throws Exception {
        //given

        //when
        List<Transaction> transactions = database.inTransaction(() -> tested.
                findAllTransactions());

        //then
        assertThat(transactions.size()).isEqualTo(3);
    }

    @Test
    public void findTransactionsFrom() throws Exception {
        //given

        //when
        List<Transaction> transactions = database.inTransaction(() -> tested.
                findTransactionsFrom(1L));

        //then
        assertThat(transactions.size()).isEqualTo(2);
    }

    @Test
    public void findTransactionsTo() throws Exception {
        //given

        //when
        List<Transaction> transactions = database.inTransaction(() -> tested.
                findTransactionsTo(1L));

        //then
        assertThat(transactions.size()).isEqualTo(1);
    }

    @Test
    public void findTransactionsFromTo() throws Exception {
        //given

        //when
        List<Transaction> transactions = database.inTransaction(() -> tested.
                findTransactionsFromTo(2L, 1L));

        //then
        assertThat(transactions.size()).isEqualTo(1);
    }

}