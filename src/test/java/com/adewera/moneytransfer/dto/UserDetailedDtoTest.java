package com.adewera.moneytransfer.dto;

import com.adewera.moneytransfer.UnitTestMarker;
import com.adewera.moneytransfer.model.Currency;
import com.adewera.moneytransfer.model.Money;
import com.adewera.moneytransfer.model.User;
import com.adewera.moneytransfer.model.Wallet;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

@Category(UnitTestMarker.class)
public class UserDetailedDtoTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private List<WalletDto> wallets;

    @Before
    public void setUp() {
        WalletDto wallet1 = new WalletDto();
        wallet1.setId(1L);
        wallet1.setCurrency("USD");
        wallet1.setAmount(BigDecimal.valueOf(10L).setScale(2));
        WalletDto wallet2 = new WalletDto();
        wallet2.setId(2L);
        wallet2.setCurrency("PLN");
        wallet2.setAmount(BigDecimal.valueOf(20L).setScale(2));
        wallets = new ArrayList<>();
        wallets.add(wallet1);
        wallets.add(wallet2);
    }

    @Test
    public void serializesToJSON() throws Exception {
        //given
        final UserDetailedDto userDetailedDto = new UserDetailedDto();
        userDetailedDto.setId(1L);
        userDetailedDto.setName("Alan");
        userDetailedDto.setSurname("Dewera");
        userDetailedDto.setEmail("alan.dewera@gmail.com");
        userDetailedDto.setWallets(wallets);

        //when
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/userDetailedDto.json"), UserDetailedDto.class));
        //then
        assertThat(MAPPER.writeValueAsString(userDetailedDto)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        //given
        final UserDetailedDto expected = new UserDetailedDto();
        expected.setId(1L);
        expected.setName("Alan");
        expected.setSurname("Dewera");
        expected.setEmail("alan.dewera@gmail.com");
        expected.setWallets(wallets);

        //when
        UserDetailedDto actual = MAPPER.readValue(fixture("fixtures/userDetailedDto.json"), UserDetailedDto.class);

        //then
        assertThat(actual.getId()).isEqualTo(expected.getId());
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getSurname()).isEqualTo(expected.getSurname());
        assertThat(actual.getEmail()).isEqualTo(expected.getEmail());
        assertThat(actual.getWallets().get(0).getCurrency()).isEqualTo(expected.getWallets().get(0).getCurrency());
    }

    @Test
    public void fromEntityMappingTest() throws Exception {
        //given
        final User user = new User("Alan", "Dewera", "alan.dewera@gmail.com");
        user.setId(1L);
        Wallet wallet = new Wallet();
        wallet.setId(2L);
        wallet.setMoney(new Money(Currency.BTC, BigDecimal.ONE.setScale(2)));
        user.addWallet(wallet);

        //when
        UserDetailedDto result = UserDetailedDto.fromEntity(user);

        //then
        assertThat(result.getName()).isEqualTo("Alan");
        assertThat(result.getSurname()).isEqualTo("Dewera");
        assertThat(result.getEmail()).isEqualTo("alan.dewera@gmail.com");
        assertThat(result.getId()).isEqualTo(1L);
        assertThat(result.getWallets().get(0).getCurrency()).isEqualTo("BTC");
        assertThat(result.getWallets().get(0).getAmount()).isEqualTo(BigDecimal.ONE.setScale(2));
        assertThat(result.getWallets().get(0).getId()).isEqualTo(2L);
    }
}