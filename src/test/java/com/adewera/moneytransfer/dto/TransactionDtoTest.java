package com.adewera.moneytransfer.dto;

import com.adewera.moneytransfer.UnitTestMarker;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Set;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@Category(UnitTestMarker.class)
public class TransactionDtoTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Validator validator;

    private TransactionDto tested;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        tested = new TransactionDto();
        tested.setWalletIdTo(2L);
        tested.setWalletIdFrom(1L);
        tested.setId(1L);
        tested.setTimestamp(Timestamp.from(Instant.ofEpochSecond(0)));
        tested.setCurrency("USD");
        tested.setAmount(BigDecimal.valueOf(10.00).setScale(2));
    }

    @Test
    public void serializesToJSON() throws Exception {
        //given

        //when
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/transactionDto.json"), TransactionDto.class));

        //then
        assertThat(MAPPER.writeValueAsString(tested)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        //given

        //when
        TransactionDto actual = MAPPER.readValue(fixture("fixtures/transactionDto.json"), TransactionDto.class);

        //then
        assertThat(actual.getId()).isEqualTo(tested.getId());
        assertThat(actual.getWalletIdFrom()).isEqualTo(tested.getWalletIdFrom());
        assertThat(actual.getWalletIdTo()).isEqualTo(tested.getWalletIdTo());
        assertThat(actual.getCurrency()).isEqualTo(tested.getCurrency());
        assertThat(actual.getAmount()).isEqualTo(tested.getAmount());
    }

    @Test
    public void validationPassed() throws Exception {
        //given

        //when
        Set<ConstraintViolation<TransactionDto>> violations = validator.validate(tested);

        //then
        assertTrue(violations.isEmpty());
    }

    @Test
    public void amountValidationNokToolongFractional() throws Exception {
        //given

        //when
        tested.setAmount(BigDecimal.valueOf(10.00).setScale(3));

        //then
        Set<ConstraintViolation<TransactionDto>> violations = validator.validate(tested);
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void amountValidationNokTooBigNumber() throws Exception {
        //given

        //when
        tested.setAmount(BigDecimal.valueOf(1000000.00).setScale(2));

        //then
        Set<ConstraintViolation<TransactionDto>> violations = validator.validate(tested);
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void amountValidationNokNegativeNumber() throws Exception {
        //given

        //when
        tested.setAmount(BigDecimal.valueOf(-1).setScale(2));

        //then
        Set<ConstraintViolation<TransactionDto>> violations = validator.validate(tested);
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void currencyValidationFailed() throws Exception {
        //given

        //when
        tested.setCurrency("XRP");

        //then
        Set<ConstraintViolation<TransactionDto>> violations = validator.validate(tested);
        assertThat(violations.size()).isEqualTo(1);
    }
}