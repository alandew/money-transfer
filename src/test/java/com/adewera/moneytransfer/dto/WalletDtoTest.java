package com.adewera.moneytransfer.dto;

import com.adewera.moneytransfer.UnitTestMarker;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.math.BigDecimal;
import java.util.Set;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@Category(UnitTestMarker.class)
public class WalletDtoTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Validator validator;

    private WalletDto tested;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        tested = new WalletDto();
        tested.setId(1L);
        tested.setCurrency("USD");
        tested.setAmount(BigDecimal.valueOf(10.00).setScale(2));
    }

    @Test
    public void serializesToJSON() throws Exception {
        //given

        //when
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/walletDto.json"), WalletDto.class));

        //then
        assertThat(MAPPER.writeValueAsString(tested)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        //given

        //when
        WalletDto actual = MAPPER.readValue(fixture("fixtures/walletDto.json"), WalletDto.class);

        //then
        assertThat(actual.getId()).isEqualTo(tested.getId());
        assertThat(actual.getCurrency()).isEqualTo(tested.getCurrency());
        assertThat(actual.getAmount()).isEqualTo(tested.getAmount());
    }

    @Test
    public void validationPassed() throws Exception {
        //given

        //when
        Set<ConstraintViolation<WalletDto>> violations = validator.validate(tested);

        //then
        assertTrue(violations.isEmpty());
    }

    @Test
    public void amountValidationNokToolongFractional() throws Exception {
        //given

        //when
        tested.setAmount(BigDecimal.valueOf(10.00).setScale(3));

        //then
        Set<ConstraintViolation<WalletDto>> violations = validator.validate(tested);
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void amountValidationNokTooBigNumber() throws Exception {
        //given

        //when
        tested.setAmount(BigDecimal.valueOf(1000000.00).setScale(2));

        //then
        Set<ConstraintViolation<WalletDto>> violations = validator.validate(tested);
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void amountValidationNokNegativeNumber() throws Exception {
        //given

        //when
        tested.setAmount(BigDecimal.valueOf(-1).setScale(2));

        //then
        Set<ConstraintViolation<WalletDto>> violations = validator.validate(tested);
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void currencyValidationFailed() throws Exception {
        //given

        //when
        tested.setCurrency("XRP");

        //then
        Set<ConstraintViolation<WalletDto>> violations = validator.validate(tested);
        assertThat(violations.size()).isEqualTo(1);
    }
}