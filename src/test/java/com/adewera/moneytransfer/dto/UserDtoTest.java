package com.adewera.moneytransfer.dto;

import com.adewera.moneytransfer.UnitTestMarker;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

@Category(UnitTestMarker.class)
public class UserDtoTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializesToJSON() throws Exception {
        //given
        final UserDto userDto = new UserDto();
        userDto.setId(1L);
        userDto.setName("Alan");
        userDto.setSurname("Dewera");
        userDto.setEmail("alan.dewera@gmail.com");

        //when
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/userDto.json"), UserDto.class));

        //then
        assertThat(MAPPER.writeValueAsString(userDto)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        //given
        final UserDto expected = new UserDto();
        expected.setId(1L);
        expected.setName("Alan");
        expected.setSurname("Dewera");
        expected.setEmail("alan.dewera@gmail.com");

        //when
        UserDto actual = MAPPER.readValue(fixture("fixtures/userDto.json"), UserDto.class);

        //then
        assertThat(actual.getId()).isEqualTo(expected.getId());
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getSurname()).isEqualTo(expected.getSurname());
        assertThat(actual.getEmail()).isEqualTo(expected.getEmail());
    }
}