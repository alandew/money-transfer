package com.adewera.moneytransfer.dao;

import com.adewera.moneytransfer.UnitTestMarker;
import com.adewera.moneytransfer.model.Transaction;
import com.adewera.moneytransfer.model.User;
import com.adewera.moneytransfer.model.Wallet;
import com.adewera.moneytransfer.repository.UserRepository;
import io.dropwizard.testing.junit.DAOTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@Category(UnitTestMarker.class)
public class UserRepositoryTest {

    @Rule
    public final DAOTestRule database = DAOTestRule.newBuilder().
            addEntityClass(User.class).
            addEntityClass(Wallet.class).
            addEntityClass(Transaction.class).
            build();

    private UserRepository tested;

    @Before
    public void setUp() {
        tested = new UserRepository(database.getSessionFactory());
    }

    @Test
    public void saveUser() throws Exception {
        //given

        //when
        Long userId =
                database.inTransaction(() -> tested.
                        saveUser(new User("Alan", "Dewera", "alan.dewera@gmail.com"))).getId();

        //then
        assertThat(userId).isNotNull();

    }

    @Test
    public void findUserFound() throws Exception {
        //given
        Long userId = 1L;

        //when
        Optional<User> found = database.inTransaction(() -> tested.findUser(userId));

        //then
        assertThat(found.isPresent()).isTrue();
        assertThat(found.get().getName()).isEqualTo("A");
    }

    @Test
    public void findUserNotFound() throws Exception {
        //given
        Long userId = 5L;

        //when
        Optional<User> found = database.inTransaction(() -> tested.findUser(userId));

        //then
        assertThat(found.isPresent()).isFalse();
    }

    @Test
    public void findAllUsers() throws Exception {
        //given

        //when
        List<User> allUsers = database.inTransaction(() -> tested.findAllUsers());

        //then
        assertThat(allUsers.size()).isEqualTo(2);

    }

    @Test
    public void deleteUser() throws Exception {
        //given
        User user = database.inTransaction(() -> tested.findUser(1L)).get();
        //when
        database.inTransaction(() -> tested.delete(user));

        //then
        assertThat(database.inTransaction(() -> tested.findAllUsers()).size()).isEqualTo(1);
    }

    @Test
    public void updateUser() throws Exception {
        //given
        User user = database.inTransaction(() -> tested.findUser(1L).get());

        //when
        user.setName("newName");
        user.setSurname("newSurname");
        database.inTransaction(() -> tested.update(user));

        //then
        User user1 = database.inTransaction(() -> tested.findUser(1L).get());
        assertThat(user1.getName()).isEqualTo("newName");
        assertThat(user1.getSurname()).isEqualTo("newSurname");
    }

    @Test
    public void findUserByWalletIdTest() throws Exception {
        //given

        //when
        Optional<User> user1 = database.inTransaction(() -> tested.findByWalletId(1L));
        Optional<User> user2 = database.inTransaction(() -> tested.findByWalletId(2L));
        Optional<User> user3 = database.inTransaction(() -> tested.findByWalletId(3L));
        Optional<User> user4 = database.inTransaction(() -> tested.findByWalletId(4L));

        //then
        assertThat(user1.get().getId()).isEqualTo(1L);
        assertThat(user2.get().getId()).isEqualTo(1L);
        assertThat(user3.get().getId()).isEqualTo(2L);
        assertThat(user4.get().getId()).isEqualTo(2L);

    }

}