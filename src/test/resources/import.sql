INSERT INTO users (user_id, name, surname, email) VALUES(1, 'A', 'A', 'alan.dewera@gmail.com')
INSERT INTO users (user_id, name, surname, email) VALUES(2, 'B', 'B', 'alan.dewera@gmail.com')

INSERT INTO wallets (wallet_id, amount, currency, user_id) VALUES(1, '10.00', 'PLN', 1)
INSERT INTO wallets (wallet_id, amount, currency, user_id) VALUES(2, '10.00', 'USD', 1)

INSERT INTO wallets (wallet_id, amount, currency, user_id) VALUES(3, '10.00', 'PLN', 2)
INSERT INTO wallets (wallet_id, amount, currency, user_id) VALUES(4, '10.00', 'USD', 2)

INSERT INTO transactions (id, wallet_id_from, wallet_id_to, currency, amount, transaction_time) VALUES(1, 1, 2, 'USD', 10.00, '2017-11-15 15:30:14.332')
INSERT INTO transactions (id, wallet_id_from, wallet_id_to, currency, amount, transaction_time) VALUES(2, 1, 2, 'GBP', 10.00, '2017-11-15 15:30:14.332')
INSERT INTO transactions (id, wallet_id_from, wallet_id_to, currency, amount, transaction_time) VALUES(3, 2, 1, 'BTC', 10.00, '2017-11-15 15:30:14.332')
