package com.adewera.moneytransfer.controller;

import com.adewera.moneytransfer.dto.UserDetailedDto;
import com.adewera.moneytransfer.dto.UserDto;
import com.adewera.moneytransfer.dto.WalletDto;
import com.adewera.moneytransfer.service.UserService;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.stream.Collectors;

@Path("/users")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;

    @Inject
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @POST
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(@Valid final UserDto userInputDto, @Context UriInfo uriInfo) {
        LOGGER.info("createUser {}", userInputDto);
        Long userId = userService.createUser(userInputDto);
        LOGGER.info("user has been created id={}", userId);
        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Long.toString(userId));
        return Response.created(builder.build()).build();
    }

    @GET
    @UnitOfWork
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserDetailedDto getUser(@PathParam("id") @NotNull final Long userId) {
        LOGGER.info("getUser userId={}", userId);
        return UserDetailedDto.fromEntity(userService.getUser(userId));
    }

    @GET
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserDto> getUsers() {
        LOGGER.info("getUsers");
        return userService.getAllUsers().
                stream().
                map(UserDto::fromEntity).
                collect(Collectors.toList());
    }

    @PUT
    @UnitOfWork
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("id") @NotNull final Long userId,
            @Valid final UserDto userInputDto) {
        LOGGER.info("updateUser userId={} {}", userId, userInputDto);
        userService.updateUser(userId, userInputDto);
        return Response.ok().build();
    }

    @DELETE
    @UnitOfWork
    @Path("{id}")
    public Response deleteUser(@PathParam("id") @NotNull final Long userId) {
        LOGGER.info("deleteUser userId={}", userId);
        userService.deleteUser(userId);
        return Response.ok().build();
    }

    @POST
    @Path("{id}/wallets")
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createWallet(@PathParam("id") @NotNull final Long userId,
            @Valid final WalletDto walletDto,
            @Context UriInfo uriInfo) {
        LOGGER.info("createWallet userId={} {}", userId, walletDto);
        Long walletId = userService.createWallet(userId, walletDto);
        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Long.toString(walletId));
        return Response.created(builder.build()).build();
    }

    @DELETE
    @Path("{id}/wallets/{walletId}")
    @UnitOfWork
    public Response deleteWallet(@PathParam("id") @NotNull final Long userId,
            @PathParam("walletId") @NotNull final Long walletId) {
        LOGGER.info("deleteWallet userId={} walletId={}", userId, walletId);
        userService.deleteWallet(userId, walletId);
        return Response.ok().build();
    }

    @PUT
    @Path("{id}/wallets/{walletId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response depositWallet(@PathParam("id") @NotNull final Long userId,
            @PathParam("walletId") @NotNull final Long walletId,
            @Valid final WalletDto walletDto) {
        LOGGER.info("depositWallet userId={} walletId={} {}", userId, walletId, walletDto);
        userService.deposit(userId, walletId, walletDto);
        return Response.ok().build();
    }
}
