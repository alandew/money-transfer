package com.adewera.moneytransfer.controller;

import com.adewera.moneytransfer.dto.TransactionDto;
import com.adewera.moneytransfer.service.TransactionService;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.stream.Collectors;

@Path("/transactions")
public class TransactionController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionController.class);

    private final TransactionService transactionService;

    @Inject
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @POST
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTransaction(@Valid final TransactionDto transactionDto, @Context UriInfo uriInfo) {
        LOGGER.info("createTransaction {}", transactionDto);
        Long transactionId = transactionService.createTransaction(transactionDto);
        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Long.toString(transactionId));
        return Response.created(builder.build()).build();
    }

    @GET
    @Path("{id}")
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    public TransactionDto getTransaction(@PathParam("id") @NotNull final Long transactionId) {
        LOGGER.info("getTransaction transactionId={}", transactionId);
        return TransactionDto.fromEntity(transactionService.getTransaction(transactionId));
    }

    @GET
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    public List<TransactionDto> getTransactions(
            @QueryParam("from-wallet") Long fromWallet,
            @QueryParam("to-wallet") Long toWallet) {
        LOGGER.info("getTransactions fromWalletId={} toWalletId={}", fromWallet, toWallet);
        return transactionService.getTransactions(fromWallet, toWallet).
                stream().
                map(TransactionDto::fromEntity).
                collect(Collectors.toList());
    }
}
