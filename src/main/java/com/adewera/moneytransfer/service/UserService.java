package com.adewera.moneytransfer.service;

import com.adewera.moneytransfer.dto.UserDto;
import com.adewera.moneytransfer.dto.WalletDto;
import com.adewera.moneytransfer.model.Currency;
import com.adewera.moneytransfer.model.Money;
import com.adewera.moneytransfer.model.User;
import com.adewera.moneytransfer.model.Wallet;
import com.adewera.moneytransfer.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.List;

public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    @Inject
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * @param userDto input argument describing user
     * @return created user unique index
     */
    public Long createUser(UserDto userDto) {
        User entity = User.fromDto(userDto);
        return userRepository.saveUser(entity).getId();
    }

    /**
     * @param userId unique user index
     * @return user entity
     * @throws NotFoundException when user with passed id not exists
     */
    public User getUser(final Long userId) {
        return findUser(userId);
    }

    /**
     * @return All user entities
     */
    public List<User> getAllUsers() {
        return userRepository.findAllUsers();
    }

    /**
     * @param userId unique user index
     * @throws NotFoundException when user with passed id not exists
     */
    public void deleteUser(Long userId) {
        User user = findUser(userId);
        userRepository.delete(user);
        LOGGER.info("User has been deleted id={}", userId);
    }

    /**
     * @param userId       unique user index
     * @param userInputDto input argument describing user
     * @throws NotFoundException when user with passed id not exists
     */
    public void updateUser(Long userId, UserDto userInputDto) {
        User user = findUser(userId);
        user.setName(userInputDto.getName());
        user.setSurname(userInputDto.getSurname());
        user.setEmail(userInputDto.getEmail());
        userRepository.update(user);
        LOGGER.info("User has been updated {}", user);
    }

    /**
     * @param userId    unique user index
     * @param walletDto input argument describing wallet to create
     * @return unique id of created wallet
     * @throws NotFoundException   when user with passed id not exists
     * @throws BadRequestException when User with passed id already has such a wallet
     */
    public Long createWallet(Long userId, WalletDto walletDto) {
        User user = findUser(userId);
        if (walletAlreadyExists(user, walletDto)) {
            LOGGER.error("User with id=" + userId + " already has " + walletDto.getCurrency() + " wallet");
            throw new BadRequestException(
                    "User with id=" + userId + " already has " + walletDto.getCurrency() + " wallet");
        }
        Currency currency = Currency.from(walletDto.getCurrency());
        Wallet wallet = new Wallet();
        wallet.setMoney(new Money(currency, walletDto.getAmount()));
        user.addWallet(wallet);
        userRepository.update(user);
        LOGGER.info("Wallet has been created userId={} {} ", userId, walletDto);
        return wallet.getId();
    }

    /**
     * @param userId   unique user index
     * @param walletId unique wallet index
     * @throws NotFoundException when user with passed id not exists or user doesn't own wallet"
     */
    public void deleteWallet(Long userId, Long walletId) {
        User user = findUser(userId);
        Wallet wallet = findWallet(user, walletId);
        user.getWallets().remove(wallet);
        userRepository.update(user);
        LOGGER.info("Wallet has been deleted userId={} walletId={} ", userId, walletId);

    }

    /**
     * @param userId    unique user index
     * @param walletId  unique wallet index
     * @param walletDto input argument describing amount money to deposit
     * @throws NotFoundException   when user with passed id not exists or user doesn't own wallet"
     * @throws BadRequestException when currencies are incompatible between wallet and money to deposit
     */
    public void deposit(Long userId, Long walletId, WalletDto walletDto) {
        User user = findUser(userId);
        Wallet wallet = findWallet(user, walletId);
        if (!wallet.getMoney().getCurrency().toString().equals(walletDto.getCurrency())) {
            LOGGER.error("Wallet=" + walletId + " is incompatible with currency" + walletDto.getCurrency());
            throw new BadRequestException(
                    "Wallet=" + walletId + " is incompatible with currency" + walletDto.getCurrency());
        }
        wallet.deposit(walletDto.getAmount());
        userRepository.update(user);
        LOGGER.info("Wallet supplied. UserId={} walletId={} {}", userId, walletId, wallet);
    }

    private User findUser(Long userId) {
        return userRepository.findUser(userId)
                .orElseThrow(() -> {
                    LOGGER.warn("User with id=" + userId + " not found");
                    return new NotFoundException("User with id=" + userId + " not found");
                });
    }

    private Wallet findWallet(User user, Long walletId) {
        return user.getWallets().
                stream().
                filter(t -> t.getId().equals(walletId)).
                findFirst().
                orElseThrow(() -> {
                    LOGGER.error("user with id=" + user.getId() + " doesn't own wallet with id=" + walletId);
                    return new NotFoundException(
                            "user with id=" + user.getId() + " doesn't own wallet with id=" + walletId);
                });
    }

    private boolean walletAlreadyExists(User user, WalletDto walletDto) {
        long count = user.getWallets().
                stream().
                filter(t -> t.getMoney().getCurrency().toString().equals(walletDto.getCurrency()))
                .count();
        return count > 0;
    }
}
