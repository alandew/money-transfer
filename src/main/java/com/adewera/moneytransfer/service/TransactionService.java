package com.adewera.moneytransfer.service;

import com.adewera.moneytransfer.dto.TransactionDto;
import com.adewera.moneytransfer.model.*;
import com.adewera.moneytransfer.repository.TransactionRepository;
import com.adewera.moneytransfer.repository.UserRepository;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class TransactionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionService.class);

    private final TransactionRepository transactionRepository;
    private final UserRepository userRepository;
    private final SessionFactory sessionFactory;

    @Inject
    public TransactionService(TransactionRepository transactionRepository, UserRepository userRepository,
                              SessionFactory sessionFactory) {
        this.transactionRepository = transactionRepository;
        this.userRepository = userRepository;
        this.sessionFactory = sessionFactory;
    }

    /**
     * @param transactionDto input argument describing transaction
     * @return created transaction unique index
     * @throws BadRequestException when source wallet doesn't have enough money to perform transaction
     * @throws NotFoundException   when source walletId or target walletId not exists or currencies are incompatible
     */
    public Long createTransaction(TransactionDto transactionDto) {
        Long sourceUserId = findUserIdByWalletId(transactionDto.getWalletIdFrom());
        Long targetUserId = findUserIdByWalletId(transactionDto.getWalletIdTo());
        //lock in the same order to avoid deadlock
        User sourceUser = null;
        User targetUser = null;
        if (sourceUserId < targetUserId) {
            sourceUser = sessionFactory.getCurrentSession().get(User.class, sourceUserId, LockMode.PESSIMISTIC_WRITE);
            targetUser = sessionFactory.getCurrentSession().get(User.class, targetUserId, LockMode.PESSIMISTIC_WRITE);
        } else {
            targetUser = sessionFactory.getCurrentSession().get(User.class, targetUserId, LockMode.PESSIMISTIC_WRITE);
            sourceUser = sessionFactory.getCurrentSession().get(User.class, sourceUserId, LockMode.PESSIMISTIC_WRITE);
        }
        Wallet sourceWallet = getWalletFromUser(sourceUser, transactionDto.getWalletIdFrom());
        Wallet targetWallet = getWalletFromUser(targetUser, transactionDto.getWalletIdTo());
        validateCurrencyCompatibility(transactionDto.getCurrency(),
                sourceWallet.getMoney().getCurrency().toString(),
                targetWallet.getMoney().getCurrency().toString());
        BigDecimal sourceAmount = sourceWallet.getMoney().getAmount();
        BigDecimal targetAmount = targetWallet.getMoney().getAmount();
        BigDecimal toTransfer = transactionDto.getAmount();
        if (sourceAmount.compareTo(toTransfer) < 0) {
            LOGGER.error("Not enough supply to perform transaction");
            throw new BadRequestException("Not enough supply to perform transaction");
        }
        sourceWallet.getMoney().setAmount(sourceAmount.subtract(toTransfer));
        targetWallet.getMoney().setAmount(targetAmount.add(toTransfer));
        Transaction transaction = new Transaction();
        transaction.setWalletIdFrom(transactionDto.getWalletIdFrom());
        transaction.setWalletIdTo(transactionDto.getWalletIdTo());
        transaction.setMoney(new Money(Currency.from(transactionDto.getCurrency()), toTransfer));
        transaction.setTimestamp(new Timestamp(System.currentTimeMillis()));
        transactionRepository.saveTransaction(transaction);
        userRepository.saveUser(sourceUser);
        userRepository.saveUser(targetUser);
        LOGGER.info("The transaction completed successfully {}", transaction);
        return transaction.getId();
    }

    /**
     * @param transactionId unique transaction index
     * @return transaction entity
     * @throws NotFoundException when transaction with passed transaction id not exists
     */
    public Transaction getTransaction(Long transactionId) {
        return findTransaction(transactionId);
    }

    /**
     * @param sourceWalletId unique source wallet index. This value can be Null.
     * @param targetWalletId unique target wallet index. This value can be Null.
     * @return Filtered transaction list
     * @throws NotFoundException when transaction with passed transaction id not exists
     */
    public List<Transaction> getTransactions(@Nullable Long sourceWalletId, @Nullable Long targetWalletId) {
        if (isNoFilter(sourceWalletId, targetWalletId)) {
            return transactionRepository.findAllTransactions();
        } else if (isSourceFilter(sourceWalletId, targetWalletId)) {
            return transactionRepository.findTransactionsFrom(sourceWalletId);
        } else if (isTargetFilter(sourceWalletId, targetWalletId)) {
            return transactionRepository.findTransactionsTo(targetWalletId);
        } else {
            return transactionRepository.findTransactionsFromTo(sourceWalletId, targetWalletId);
        }
    }

    private boolean isNoFilter(Long fromWalletId, Long toWalletId) {
        return fromWalletId == null && toWalletId == null;
    }

    private boolean isSourceFilter(Long fromWalletId, Long toWalletId) {
        return fromWalletId != null && toWalletId == null;
    }

    private boolean isTargetFilter(Long fromWalletId, Long toWalletId) {
        return fromWalletId == null && toWalletId != null;
    }

    private void validateCurrencyCompatibility(String dtoCurrency, String sourceCurrency, String targetCurrency) {
        if (!dtoCurrency.equals(sourceCurrency) || !sourceCurrency.equals(targetCurrency)) {
            throw new BadRequestException(
                    "Currencies: " + dtoCurrency + " " + sourceCurrency + " " + targetCurrency + "are incompatible");
        }
    }

    private User findUserByWalletId(Long walletId) {
        return userRepository.findByWalletId(walletId)
                .orElseThrow(() -> new NotFoundException("There is no user with walletId=" + walletId));
    }

    private Long findUserIdByWalletId(Long walletId) {
        return findUserByWalletId(walletId).getId();
    }

    private Wallet getWalletFromUser(User user, Long walletId) {
        //We are sure that there is walletId inside user
        return user.getWallets().
                stream().
                filter(t -> t.getId().equals(walletId)).
                findFirst().
                get();
    }

    private Transaction findTransaction(Long transactionId) {
        return transactionRepository.findTransaction(transactionId).
                orElseThrow(() -> new NotFoundException("Transaction with id=" + transactionId + " not found"));

    }
}
