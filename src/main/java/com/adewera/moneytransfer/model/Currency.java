package com.adewera.moneytransfer.model;

public enum Currency {
    USD,
    PLN,
    GBP,
    BTC,;

    public static Currency from(String currency) {
        switch (currency.toUpperCase()) {
            case "USD":
                return USD;
            case "PLN":
                return PLN;
            case "GBP":
                return GBP;
            case "BTC":
                return BTC;
            default:
                throw new IllegalArgumentException();
        }
    }
}
