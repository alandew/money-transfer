package com.adewera.moneytransfer.model;

import javax.persistence.*;
import java.sql.Timestamp;

@NamedQueries({
        @NamedQuery(name = "Transaction.findAll",
                query = "SELECT t FROM Transaction t"),
        @NamedQuery(name = "Transaction.findFrom",
                query = "SELECT t FROM Transaction t " +
                        "WHERE t.walletIdFrom = :walletIdFrom"),
        @NamedQuery(name = "Transaction.findTo",
                query = "SELECT t FROM Transaction t " +
                        "WHERE t.walletIdTo = :walletIdTo"),
        @NamedQuery(name = "Transaction.findFromTo",
                query = "SELECT t FROM Transaction t " +
                        "WHERE t.walletIdFrom = :walletIdFrom and t.walletIdTo = :walletIdTo")
})
@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "wallet_id_from", nullable = false)
    private Long walletIdFrom;

    @Column(name = "wallet_id_to", nullable = false)
    private Long walletIdTo;

    @Embedded
    private Money money;

    @Column(name = "transaction_time", nullable = false)
    private Timestamp timestamp;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getWalletIdFrom() {
        return walletIdFrom;
    }

    public void setWalletIdFrom(Long walletIdFrom) {
        this.walletIdFrom = walletIdFrom;
    }

    public Long getWalletIdTo() {
        return walletIdTo;
    }

    public void setWalletIdTo(Long walletIdTo) {
        this.walletIdTo = walletIdTo;
    }

    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", walletIdFrom=" + walletIdFrom +
                ", walletIdTo=" + walletIdTo +
                ", money=" + money +
                ", timestamp=" + timestamp +
                '}';
    }
}
