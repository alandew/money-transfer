package com.adewera.moneytransfer.repository;

import com.adewera.moneytransfer.model.User;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class UserRepository extends AbstractDAO<User> {

    @Inject
    public UserRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public User saveUser(User user) {
        return persist(user);
    }

    public Optional<User> findUser(Long id) {
        return Optional.ofNullable(get(id));
    }

    @SuppressWarnings("unchecked")
    public List<User> findAllUsers() {
        return list((Query<User>) namedQuery("User.findAll"));
    }

    public void delete(User user) {
        currentSession().delete(user);
    }

    public void update(User user) {
        currentSession().update(user);
        currentSession().flush();
    }

    @SuppressWarnings("unchecked")
    public Optional<User> findByWalletId(Long walletId) {
        return Optional.ofNullable(
                uniqueResult(
                        namedQuery("User.findByWalletId")
                                .setParameter("walletId", walletId)
                ));
    }
}
