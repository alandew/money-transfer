package com.adewera.moneytransfer.repository;

import com.adewera.moneytransfer.model.Transaction;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class TransactionRepository extends AbstractDAO<Transaction> {

    @Inject
    public TransactionRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Transaction saveTransaction(Transaction transaction) {
        return persist(transaction);
    }

    public Optional<Transaction> findTransaction(Long transactionId) {
        return Optional.ofNullable(get(transactionId));
    }

    @SuppressWarnings("unchecked")
    public List<Transaction> findAllTransactions() {
        return list((Query<Transaction>) namedQuery("Transaction.findAll"));
    }

    @SuppressWarnings("unchecked")
    public List<Transaction> findTransactionsFrom(Long walletIdFrom) {
        return list((Query<Transaction>) namedQuery("Transaction.findFrom").
                setParameter("walletIdFrom", walletIdFrom));
    }

    @SuppressWarnings("unchecked")
    public List<Transaction> findTransactionsTo(Long walletIdTo) {
        return list((Query<Transaction>) namedQuery("Transaction.findTo").
                setParameter("walletIdTo", walletIdTo));
    }

    @SuppressWarnings("unchecked")
    public List<Transaction> findTransactionsFromTo(Long walletIdFrom, Long walletIdTo) {
        return list((Query<Transaction>) namedQuery("Transaction.findFromTo").
                setParameter("walletIdFrom", walletIdFrom).
                setParameter("walletIdTo", walletIdTo));
    }
}
