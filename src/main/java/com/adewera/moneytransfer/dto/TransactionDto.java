package com.adewera.moneytransfer.dto;

import com.adewera.moneytransfer.dto.validation.CurrencyCheck;
import com.adewera.moneytransfer.model.Transaction;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class TransactionDto {

    @JsonProperty
    private long id;

    @JsonProperty(required = true)
    @NotNull
    private Long walletIdFrom;

    @JsonProperty(required = true)
    @NotNull
    private Long walletIdTo;

    @NotEmpty(message = "can not be empty")
    @JsonProperty(required = true)
    @CurrencyCheck
    private String currency;

    @JsonProperty(required = true)
    @DecimalMin(value = "0", inclusive = false)
    @Digits(integer = 6, fraction = 2)
    @NotNull
    private BigDecimal amount;

    @JsonProperty
    private Timestamp timestamp;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getWalletIdFrom() {
        return walletIdFrom;
    }

    public void setWalletIdFrom(Long walletIdFrom) {
        this.walletIdFrom = walletIdFrom;
    }

    public Long getWalletIdTo() {
        return walletIdTo;
    }

    public void setWalletIdTo(Long walletIdTo) {
        this.walletIdTo = walletIdTo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public static TransactionDto fromEntity(Transaction transaction) {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setId(transaction.getId());
        transactionDto.setWalletIdFrom(transaction.getWalletIdFrom());
        transactionDto.setWalletIdTo(transaction.getWalletIdTo());
        transactionDto.setCurrency(transaction.getMoney().getCurrency().toString());
        transactionDto.setAmount(transaction.getMoney().getAmount());
        transactionDto.setTimestamp(transaction.getTimestamp());
        return transactionDto;
    }
}
