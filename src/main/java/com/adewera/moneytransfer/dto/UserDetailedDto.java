package com.adewera.moneytransfer.dto;

import com.adewera.moneytransfer.model.User;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.stream.Collectors;

public class UserDetailedDto extends UserDto{

    @JsonProperty
    private List<WalletDto> wallets;

    public UserDetailedDto() {
        super();
    }

    public List<WalletDto> getWallets() {
        return wallets;
    }

    public void setWallets(List<WalletDto> wallets) {
        this.wallets = wallets;
    }

    public static UserDetailedDto fromEntity(User user) {
        UserDetailedDto userDto = new UserDetailedDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setSurname(user.getSurname());
        userDto.setEmail(user.getEmail());
        List<WalletDto> walletDtos = user.getWallets().
                stream().
                map( t-> {
                    WalletDto walletDto = new WalletDto();
                    walletDto.setId(t.getId());
                    walletDto.setCurrency(t.getMoney().getCurrency().toString());
                    walletDto.setAmount(t.getMoney().getAmount());
                    return walletDto;
                }).collect(Collectors.toList());
        userDto.setWallets(walletDtos);
        return userDto;
    }
}
