package com.adewera.moneytransfer.dto.validation;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.stream.Collectors;

/**
 * Created by adewera on 9/4/2018.
 */
@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
    @Override
    public Response toResponse(ConstraintViolationException e) {
        String message = e.getConstraintViolations().
                stream().
                map(ConstraintViolation::getMessage).
                collect(Collectors.joining("\n"));
        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
    }
}
