package com.adewera.moneytransfer.dto.validation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target( { METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = CurrencyCheckValidator.class)
@Documented
public @interface CurrencyCheck {

    String message() default "{com.adewera.moneytransfer.dto.validation.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}