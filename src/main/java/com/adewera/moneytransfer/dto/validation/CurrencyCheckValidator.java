package com.adewera.moneytransfer.dto.validation;

import com.adewera.moneytransfer.model.Currency;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.stream.Collectors;

public class CurrencyCheckValidator implements ConstraintValidator<CurrencyCheck, String> {

    @Override
    public void initialize(CurrencyCheck currencyCheck) {
    }

    @Override
    public boolean isValid(String object, ConstraintValidatorContext constraintContext) {
        try {
            Currency.from(object);
            return true;
        } catch (IllegalArgumentException ex) {
            constraintContext.disableDefaultConstraintViolation();
            constraintContext.buildConstraintViolationWithTemplate("Currency may be one of " +
                    Arrays.stream(Currency.values()).map(Enum::toString).collect(Collectors.joining(" ")))
                    .addPropertyNode(object)
                    .addConstraintViolation();
            return false;
        }
    }

}