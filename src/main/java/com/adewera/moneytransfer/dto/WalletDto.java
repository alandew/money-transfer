package com.adewera.moneytransfer.dto;

import com.adewera.moneytransfer.dto.validation.CurrencyCheck;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class WalletDto {

    @JsonProperty
    private Long id;

    @NotEmpty(message = "can not be empty")
    @CurrencyCheck
    @JsonProperty(required = true)
    private String currency;

    @DecimalMin(value="0", inclusive=false)
    @Digits(integer = 6, fraction = 2)
    @NotNull
    @JsonProperty(required = true)
    private BigDecimal amount;

    public WalletDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "WalletDto{" +
                "id=" + id +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                '}';
    }
}
