package com.adewera.moneytransfer.config;

import com.adewera.moneytransfer.model.Transaction;
import com.adewera.moneytransfer.model.User;
import com.adewera.moneytransfer.model.Wallet;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import org.hibernate.SessionFactory;

public class ApplicationContext extends AbstractModule {
    private final HibernateBundle<MoneyTransferConfiguration> hibernateBundle =
            new HibernateBundle<MoneyTransferConfiguration>(User.class, Wallet.class, Transaction.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(MoneyTransferConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    public ApplicationContext(Bootstrap<MoneyTransferConfiguration> bootstrap) {
        bootstrap.addBundle(hibernateBundle);
    }

    @Provides
    public SessionFactory provideSessionFactory() {
        return hibernateBundle.getSessionFactory();
    }

    @Override
    protected void configure() {
    }
}
