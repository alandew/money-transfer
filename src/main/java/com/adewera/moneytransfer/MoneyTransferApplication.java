package com.adewera.moneytransfer;

import com.adewera.moneytransfer.config.ApplicationContext;
import com.adewera.moneytransfer.config.MoneyTransferConfiguration;
import com.adewera.moneytransfer.controller.TransactionController;
import com.adewera.moneytransfer.controller.UserController;
import com.adewera.moneytransfer.dto.validation.ConstraintViolationExceptionMapper;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.dropwizard.Application;
import io.dropwizard.jersey.errors.EarlyEofExceptionMapper;
import io.dropwizard.jersey.errors.LoggingExceptionMapper;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.dropwizard.server.DefaultServerFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class MoneyTransferApplication extends Application<MoneyTransferConfiguration> {

    private Injector injector;

    public static void main(final String[] args) throws Exception {
        new MoneyTransferApplication().run(args);
    }

    @Override
    public String getName() {
        return "Money Transfer";
    }

    @Override
    public void initialize(final Bootstrap<MoneyTransferConfiguration> bootstrap) {
         injector = Guice.createInjector(new ApplicationContext(bootstrap));
    }

    @Override
    public void run(final MoneyTransferConfiguration configuration,
                    final Environment environment) {
        environment.jersey().register(injector.getInstance(UserController.class));
        environment.jersey().register(injector.getInstance(TransactionController.class));

        //Register custom constraint exception mapper and default Dropwizard mappers.
        //This is just work around. Jersey is returning 500 when Constraint violation occurs
        ((DefaultServerFactory)configuration.getServerFactory()).setRegisterDefaultExceptionMappers(false);
        environment.jersey().register(injector.getInstance(ConstraintViolationExceptionMapper.class));
        environment.jersey().register(new LoggingExceptionMapper<Throwable>() {});
        environment.jersey().register(new JsonProcessingExceptionMapper());
        environment.jersey().register(new EarlyEofExceptionMapper());
    }

}
