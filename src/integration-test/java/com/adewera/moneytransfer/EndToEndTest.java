package com.adewera.moneytransfer;

import com.adewera.moneytransfer.config.MoneyTransferConfiguration;
import com.adewera.moneytransfer.controller.UserController;
import com.adewera.moneytransfer.dto.TransactionDto;
import com.adewera.moneytransfer.dto.UserDetailedDto;
import com.adewera.moneytransfer.dto.UserDto;
import com.adewera.moneytransfer.dto.WalletDto;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

import static io.dropwizard.testing.ResourceHelpers.resourceFilePath;
import static org.assertj.core.api.Assertions.assertThat;

@Category(IntegrationTestMarker.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EndToEndTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    private static final UserDto user1 = new UserDto();
    private static final UserDto user2 = new UserDto();
    private static final WalletDto tenUSDWalletDto = new WalletDto();
    private static final WalletDto twentyUsdWalletDto = new WalletDto();
    private static Long user1Id;
    private static Long user2Id;
    private static Long wallet1Id;
    private static Long wallet2Id;
    private static Long transactionId;

    @ClassRule
    public static final DropwizardAppRule<MoneyTransferConfiguration> RULE =
            new DropwizardAppRule<>(MoneyTransferApplication.class, resourceFilePath("test-config.yml"));

    @BeforeClass
    public static void setUp() {
        user1.setName("Alan");
        user1.setSurname("Dewera");
        user1.setEmail("alan.dewera@gmail.com");

        user2.setName("Another");
        user2.setSurname("User");
        user2.setEmail("another.user@gmail.com");

        tenUSDWalletDto.setCurrency("USD");
        tenUSDWalletDto.setAmount(BigDecimal.valueOf(10).setScale(2));

        twentyUsdWalletDto.setCurrency("USD");
        twentyUsdWalletDto.setAmount(BigDecimal.valueOf(20).setScale(2));
    }

    @Test(timeout = 10 * 1000)
    public void step01_createUser1() {
        //when
        Response response = createUser(user1);

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.CREATED);
        assertThat(response.getLocation().getPath()).isNotEmpty();
        user1Id = getCreatedResourceId(response.getLocation().getPath());
    }

    @Test(timeout = 10 * 1000)
    public void step02_verifyThatUser1HasBeenCreated() {
        //when
        UserDetailedDto result = getUser(user1Id);

        //then
        assertThat(result.getName()).isEqualTo("Alan");
        assertThat(result.getSurname()).isEqualTo("Dewera");
        assertThat(result.getEmail()).isEqualTo("alan.dewera@gmail.com");
        assertThat(result.getWallets()).isEmpty();
    }

    @Test(timeout = 10 * 1000)
    public void step03_createUser2() {
        //when
        Response response = createUser(user2);

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.CREATED);
        assertThat(response.getLocation().getPath()).isNotEmpty();
        user2Id = getCreatedResourceId(response.getLocation().getPath());
    }

    @Test(timeout = 10 * 1000)
    public void step04_verifyThatBothUserExists() {
        //when
        List<UserDto> users = getUsers();

        //then
        assertThat(users.size()).isEqualTo(2);
    }

    @Test(timeout = 10 * 1000)
    public void step05_createUser1WalletWith10USD() {
        //when
        Response response = createWallet(user1Id, tenUSDWalletDto);

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.CREATED);
        assertThat(response.getLocation().getPath()).isNotEmpty();
        wallet1Id = getCreatedResourceId(response.getLocation().getPath());
    }

    @Test(timeout = 10 * 1000)
    public void step06_verifyThatUser1WalletHasBeenCreated() {
        //when
        UserDetailedDto userDetailedDto = getUser(user1Id);

        //then
        assertThat(userDetailedDto.getWallets().size()).isEqualTo(1);
        assertThat(userDetailedDto.getWallets().get(0).getCurrency()).isEqualTo("USD");
        assertThat(userDetailedDto.getWallets().get(0).getAmount()).isEqualTo(BigDecimal.TEN.setScale(2));
    }

    @Test(timeout = 10 * 1000)
    public void step07_createUser2WalletWith20USD() {
        //when
        Response response = createWallet(user2Id, twentyUsdWalletDto);

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.CREATED);
        assertThat(response.getLocation().getPath()).isNotEmpty();
        wallet2Id = getCreatedResourceId(response.getLocation().getPath());
    }

    @Test(timeout = 10 * 1000)
    public void step08_verifyThatUser2WalletHasBeenCreated() {
        //when
        UserDetailedDto userDetailedDto = getUser(user2Id);

        //then
        assertThat(userDetailedDto.getWallets().size()).isEqualTo(1);
        assertThat(userDetailedDto.getWallets().get(0).getCurrency()).isEqualTo("USD");
        assertThat(userDetailedDto.getWallets().get(0).getAmount()).isEqualTo(BigDecimal.valueOf(20).setScale(2));
    }

    @Test(timeout = 10 * 1000)
    public void step09_deposit10moreUSDToUser2Wallet() {
        //when
        Response response = deposit(user2Id, wallet2Id, tenUSDWalletDto);

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
    }

    @Test(timeout = 10 * 1000)
    public void step10_verifyThatUser1WalletHasBeenUpdated() {
        //when
        UserDetailedDto response = getUser(user2Id);

        //then
        assertThat(response.getWallets().get(0).getAmount()).isEqualTo(BigDecimal.valueOf(30).setScale(2));
    }

    @Test(timeout = 10 * 1000)
    public void step11_createTransactionFromUser1ToUSer2With_9_99_USD() {
        //when
        Response response = createTransaction(wallet1Id, wallet2Id, "USD", BigDecimal.valueOf(9.99).setScale(2));

        //then
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.CREATED);
        assertThat(response.getLocation().getPath()).isNotEmpty();
        transactionId = getCreatedResourceId(response.getLocation().getPath());
    }

    @Test(timeout = 10 * 1000)
    public void step12_verifyUser1WalletState() {
        //when
        UserDetailedDto response = getUser(user1Id);

        //then
        assertThat(response.getWallets().get(0).getAmount()).isEqualTo(BigDecimal.valueOf(0.01).setScale(2));
    }

    @Test(timeout = 10 * 1000)
    public void step13_verifyUser2WalletState() {
        //when
        UserDetailedDto response = getUser(user2Id);

        //then
        assertThat(response.getWallets().get(0).getAmount()).isEqualTo(BigDecimal.valueOf(39.99).setScale(2));
    }

    @Test(timeout = 10 * 1000)
    public void step14_verifyTransactionsFromUser1Wallet() {
        //when
        List<TransactionDto> transactionsFrom = getTransactionsFrom(wallet1Id);

        //then
        assertThat(transactionsFrom.size()).isEqualTo(1);
    }

    @Test(timeout = 10 * 1000)
    public void step15_verifyTransactionsToUser2() {
        //when
        List<TransactionDto> transactionsFrom = getTransactionsTo(wallet2Id);

        //then
        assertThat(transactionsFrom.size()).isEqualTo(1);
    }

    @Test(timeout = 10 * 1000)
    public void step16_verifyTransactionsFromUser1ToUser2() {
        //when
        List<TransactionDto> transactionsFrom = getTransactionsFromTo(wallet1Id, wallet2Id);

        //then
        assertThat(transactionsFrom.size()).isEqualTo(1);
    }

    @Test(timeout = 10 * 1000)
    public void step17_verifyNoTransactionsFromUser2ToUser1() {
        //when
        List<TransactionDto> transactionsFrom = getTransactionsFromTo(wallet2Id, wallet1Id);

        //then
        assertThat(transactionsFrom).isEmpty();
    }

    private Long getCreatedResourceId(String path) {
        String[] parts = path.split("/");
        return Long.valueOf(parts[parts.length - 1]);
    }

    private Response createUser(UserDto entity) {
        return RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/v1/users")
                .request()
                .post(Entity.entity(entity, MediaType.APPLICATION_JSON_TYPE));
    }

    private UserDetailedDto getUser(Long userId) {
        return RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/v1/users/" + userId)
                .request()
                .get(UserDetailedDto.class);
    }

    @SuppressWarnings("unchecked")
    private List<UserDto> getUsers() {
        return RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/v1/users")
                .request()
                .get(List.class);
    }

    private Response createWallet(Long userId, WalletDto walletDto) {
        return RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/v1/users/" + userId + "/wallets")
                .request()
                .post(Entity.entity(walletDto, MediaType.APPLICATION_JSON_TYPE));
    }

    private Response deposit(Long userId, Long walletId, WalletDto walletDto) {
        return RULE.client()
                .target("http://localhost:" + RULE.getLocalPort() + "/api/v1/users/" + userId + "/wallets/" + walletId)
                .request()
                .put(Entity.entity(walletDto, MediaType.APPLICATION_JSON_TYPE));
    }

    private Response createTransaction(Long sourceWalletId, Long targetWalletId, String currency, BigDecimal amount) {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setCurrency(currency);
        transactionDto.setAmount(amount);
        transactionDto.setWalletIdFrom(sourceWalletId);
        transactionDto.setWalletIdTo(targetWalletId);
        return RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/v1/transactions")
                .request()
                .post(Entity.entity(transactionDto, MediaType.APPLICATION_JSON_TYPE));
    }

    @SuppressWarnings("unchecked")
    private List<TransactionDto> getTransactionsFrom(Long sourceWalletId) {
        return RULE.client()
                .target("http://localhost:" + RULE.getLocalPort() + "/api/v1/transactions?from-wallet=" + sourceWalletId
                        .toString())
                .request()
                .get(List.class);
    }

    @SuppressWarnings("unchecked")
    private List<TransactionDto> getTransactionsTo(Long targetWalletId) {
        return RULE.client()
                .target("http://localhost:" + RULE.getLocalPort() + "/api/v1/transactions?to-wallet=" + targetWalletId
                        .toString())
                .request()
                .get(List.class);
    }

    @SuppressWarnings("unchecked")
    private List<TransactionDto> getTransactionsFromTo(Long sourceWalletId, Long targetWalletId) {
        return RULE.client()
                .target("http://localhost:" + RULE.getLocalPort() + "/api/v1/transactions?to-wallet=" + targetWalletId
                        + "&from-wallet=" + sourceWalletId.toString())
                .request()
                .get(List.class);
    }
}
