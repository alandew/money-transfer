# Money Transfer
  
A Java RESTful web services for money transactions between users accounts  
The model consists of two aggregates:  
- User  
- Transaction  
Users can have wallets with different currencies.  
The transaction is only allowed between wallets with the same currency.  
The source wallet has to contain more money than requested transaction.  
  
### Requirements
- Java 8  
- Maven 3+  
  
### Technologies
The application was developed using Dropwizard Web framework  
Below are the list of libraries and technologies utilized from dropwizard in Money Transfer application  
- Jetty client/server for http. Client was used for integration test purpose  
- Jersey for REST  
- Jackson for JSON  
- Hibernate Validator for input data validation  
- Hibernate for ORM  
  
- Guice for DI  
- embedded H2 database    
- Junit 4  
- Mockito  


### REST Api documentation
Each of the following POST and PUT methods consumes JSON body. 
    
    |------------------------------------------------------------------------------------------|
	| HTTP METHOD| PATH                                     | USAGE                             | 
    |------------------------------------------------------------------------------------------|
    | POST       | /user                                    | create user                      |  
    | GET        | /user/{userId}                           | get user by Id                   |  
    | GET        | /user                                    | get all users                    |  
    | PUT        | /user/{userId}                           | update user                      |  
    | DELETE     | /user/{userId}                           | delete user with all his wallets |  
    | POST       | /user/{userId}/wallets                   | create wallet for user           |  
    | DELETE     | /user/{userId}/wallets/{walletId}        | delete wallet from user          |  
    | PUT        | /user/{userId}/wallets/{walletId}        | deposit money to wallet          |  
    |            |                                          |                                  |  
    | POST       | /transactions                            | create transaction               |  
    | GET        | /transactions/{transactionId}            | get transaction                  |  
    | GET        | /transactions/?{from-wallet}&{to-wallet} | get transactions using filtering.|  
    |            |                                          | Query parameters are optional    |  
    |------------|-----------------------------------------------------------------------------|

### Http Status
- 200 OK: The request has succeeded
- 201 Created: The resource has been created successfully
- 400 Bad Request: The eqecution of the request has failed. Reason message will be attached 
- 404 Not Found: The requested resource cannot be found  
- 500 Internal Server Error: The server encountered an unexpected condition  

### Sample JSON input messages
##### User :
```sh
{
  "name": "Alan",
  "surname": "Dewera",
  "email": "alan.dewera@gmail.com"
}
```
##### Wallet: :

```sh
{
   "currency": "USD",
   "amount": "10.00"
}
```

#### Transaction:
```sh
{
  "walletIdFrom": "1",
  "walletIdTo": "2",
  "currency": "USD",
  "amount": "10.00"
}
```

### Sample JSON messages output messages
##### User :
```sh
{
  "id": "1"
  "name": "Alan",
  "surname": "Dewera",
  "email": "alan.dewera@gmail.com"
}
```
##### Wallet: :

```sh
{
  "id": "1",
   "currency": "USD",
   "amount": "10.00"
}
```

#### Transaction:
```sh
{
  "id": "1",
  "walletIdFrom": "1",
  "walletIdTo": "2",
  "currency": "USD",
  "amount": "10.00"
  "timestamp": "1535522409386"
}
```
Currency available values are:  
- USD/PLN/GBP/BTC  

## How to verify the application
End to end test has been prepared to demonstrate that the API works as expected.  
This test triggers  each of the endpoint and each service  
The test scenario is following:  
Step 1) Create user1  
Step 2) Verify that user1 has been created  
Step 3) create user2  
Step 4) Verify that both users exist  
Step 5) Create wallet for user1 with 10 USD  
Step 6) Verify that wallet has been created  
Step 7) Create wallet for user2 with 20 USD  
Step 8) Verify that wallet has been created  
Step 9) Deposit 10 more USD to user2 wallet  
Step 10) Verify that user2 wallet has been updated  
Step 11) Crete transaction from user1 to user2 with 9.99 USD  
Step 12) Verify user1 wallet state  
Step 13) Verify user2 wallet state  
Step 14) check all transactions from user1 wallet  
Step 15) check all transactions to user2 wallet  
Step 16) check all transactions from user1 wallet to user2 wallet  
  
---
To run above test execute following commands:  
1. Run `mvn clean package` to build application  
2. Run `mvn clean verify -P integration-test` to run integration tests  
  
To run unit tests execute:  
1. mvn clean test -P dev  
  
To run standalone server execute:  
1. mvn clean package exec:java  
  
http server will be available on http://localhost:8080/api/v1  